﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix_diagonals_sum_average
{
    class Program
    {
        static void Main(string[] args)
        {
            const int dim = 5;
            int[,] matrix = new int[dim, dim];

            Populate(matrix, dim);
            Print(matrix, dim);

            Console.WriteLine();

            double principalDiagonalSum = PrincipalDiagonalSum(matrix, dim);
            double principalDiagonalAverage = DiagonalAverage(principalDiagonalSum, dim);

            SetForeground(ConsoleColor.DarkGreen);
            Console.WriteLine("MAIN DIAGONAL VALUES");
            ResetForegroundColor();
            Console.WriteLine($"Sum => {principalDiagonalSum}");
            Console.WriteLine($"Average => {principalDiagonalAverage}");

            Console.WriteLine();

            double secondaryDiagonalSum = SecondaryDiagonalSum(matrix, dim);
            double secondaryDiagonalAverage = DiagonalAverage(secondaryDiagonalSum, dim);

            SetForeground(ConsoleColor.DarkGreen);
            Console.WriteLine("SECONDARY DIAGONAL VALUES");
            ResetForegroundColor();
            Console.WriteLine($"Sum => {secondaryDiagonalSum}");
            Console.Write($"Average => {secondaryDiagonalAverage}");

            Console.ReadKey();
        }

        static void Populate(int[ , ] matrix, int dim)
        {
            Random rand = new Random();
            for (int i = 0; i < dim; i++)
            {
                for (int j = 0; j < dim; j++)
                    matrix[i, j] = rand.Next(10);
            }
        }

        static void Print(int[ , ] matrix, int dim)
        {
            SetForeground(ConsoleColor.DarkGreen);
            Console.WriteLine("MATRIX" + Environment.NewLine);
            ResetForegroundColor();
            for (int i = 0; i < dim; i++, Console.WriteLine())
            {
                for (int j = 0; j < dim; j++, Console.Write("\t"))
                {
                    if (i == j || j == dim - 1 - i)
                        SetForeground(ConsoleColor.DarkGreen);
                    else
                        ResetForegroundColor();

                    Console.Write(matrix[i, j]);
                }
            }
        }

        static double PrincipalDiagonalSum(int[ , ] matrix, int dim)
        {
            double principalDiagonalSum = 0;
            for (int i = 0; i < dim; i++)
            {
                principalDiagonalSum += matrix[i, i];
            }
            return principalDiagonalSum;
        }

        static double SecondaryDiagonalSum(int[ , ] matrix, int dim)
        {
            double secondaryDiagonalSum = 0;
            for (int i = 0, j = dim - 1; i < dim; i++, j--)
            {
                secondaryDiagonalSum += matrix[i, j];
            }
            return secondaryDiagonalSum;
        }

        static double DiagonalAverage(double diagonalSum, int n)
        {
            double average = diagonalSum / n;
            return average;
        }

        static void SetForeground(ConsoleColor color)
        {
            Console.ForegroundColor = color;
        }

        static void ResetForegroundColor()
        {
            Console.ResetColor();
        }

    }
}
